<?php
session_start();
if (!isset($_SESSION['user_name']))
{
	/* Redirect browser */
header("Location: index.php");
/* Make sure that code below does not get executed when we redirect. */
exit;
}


?>
<?php
	$error = "";
	$msg = "";
	$fileElementName = 'fileToUpload';
	if(!empty($_FILES[$fileElementName]['error']))
	{
		switch($_FILES[$fileElementName]['error'])
		{

			case '1':
				$error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
				break;
			case '2':
				$error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
				break;
			case '3':
				$error = 'The uploaded file was only partially uploaded';
				break;
			case '4':
				$error = 'No file was uploaded.';
				break;

			case '6':
				$error = 'Missing a temporary folder';
				break;
			case '7':
				$error = 'Failed to write file to disk';
				break;
			case '8':
				$error = 'File upload stopped by extension';
				break;
			case '999':
			default:
				$error = 'No error code avaiable';
		}
	}elseif(empty($_FILES['fileToUpload']['tmp_name']) || $_FILES['fileToUpload']['tmp_name'] == 'none')
	{
		$error = 'No file was uploaded..';
	}else 
	{		$file_name_= date('YmdHis#').$_FILES['fileToUpload']['name'];
			$output_dir="Upload/";
			$output_dir=$_SERVER['DOCUMENT_ROOT']."/MLReview/public/data/uploaded/post/";

			$msg .= " File Name: " . $_FILES['fileToUpload']['name'] . ", ";
			$msg .= " File Size: " . @filesize($_FILES['fileToUpload']['tmp_name']);
			//move the uploaded file to uploads folder;
			$allowedImgExts = array("gif", "jpeg", "jpg", "png");
			$getExt = explode(".", $_FILES['fileToUpload']['name']);

			if(in_array($getExt[1], $allowedImgExts))
			{
				move_uploaded_file($_FILES["fileToUpload"]["tmp_name"],$output_dir. $_FILES['fileToUpload']['name']);
				// $file_ext=substr($_FILES['fileToUpload']['name'], 14);
				// $file_date=substr($_FILES['fileToUpload']['name'], 0,8);
				// $file_name=substr($_FILES['fileToUpload']['name'], 8,6);
				//$basepath=$_SERVER['DOCUMENT_ROOT']."/test2/";
				//$basepath=$_SERVER['DOCUMENT_ROOT']."/MLReview/public/data/logsync/Logs/";
				/*if (!file_exists($basepath.$file_date)) 
				{
			    	mkdir($basepath.$file_date, 0777, true);
				}*/
				//move_uploaded_file($_FILES["fileToUpload"]["tmp_name"],$basepath.$file_date.'/'. $file_name.$file_ext);

				/*$date=substr($file_date, 0,4).'-'.substr($file_date, 4,2).'-'.substr($file_date, 6,2);
				$time=substr($file_name, 0,2).':'.substr($file_name, 2,2).':'.substr($file_name, 4,2);

				if (!file_exists($basepath."images.xml")) 
				{
				    	$xml .="<images xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" creationTime=\"".date('Y-m-d H:i:s')."\">\n\t\t";
					
	                    $xml .= "<image similarity=\"0\" groupID=\"1\">\n\t\t";

	                    $xml .= "<reviewed>".'no'."</reviewed>\n\t\t";

	                    $xml .= "<time>".$time."</time>\n\t\t";

	                    $xml .= "<date>".$date."</date>\n\t\t";

	                    $xml .= "<name>Logs/".$file_date."/".$file_name.$file_ext."</name>\n\t\t";

	                    $xml .= "</image>\n";

				    $xml.="</images>\n";

	    			$xmlobj=new SimpleXMLElement($xml);
	    			$xmlobj->asXML($basepath."images.xml");
				}
				else
				{

					$handle = fopen($basepath."images.xml", 'a') or die('Cannot open xml file'); 
					array_pop($handle); 
					$xml .= "";
					$xml .= "<image similarity=\"0\" groupID=\"1\">\n\t\t";

                    $xml .= "<reviewed>".'no'."</reviewed>\n\t\t";

                    $xml .= "<time>".$time."</time>\n\t\t";

                    $xml .= "<date>".$date."</date>\n\t\t";

                    $xml .= "<name>Logs/".$file_date."/".$file_name.$file_ext."</name>\n\t\t";

                    $xml .= "</image>\n";

			    	$xml.="</images>\n";	
					fwrite($handle, $xml);
					fclose($handle);
			    
				}*/
				
			}
			
			else
        	move_uploaded_file($_FILES["fileToUpload"]["tmp_name"],$output_dir. $file_name_);
			//for security reason, we force to remove all uploaded file
			//@unlink($_FILES['fileToUpload']);		
	}		
	echo "{";
	echo				"error: '" . $error . "',\n";
	echo				"msg: '" . $msg . "'\n";
	echo "}";
?>