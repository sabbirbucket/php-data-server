<?php
session_start();
//echo "Nada".$_SESSION['views'];
if (!isset($_SESSION['user_name']))
{
	/* Redirect browser */
header("Location: index.php");
/* Make sure that code below does not get executed when we redirect. */
exit;
}


?>
<html>
	<head>
		<title>File Uploader</title>
<link href="upload_style.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="jquery.js"></script>
	<script type="text/javascript" src="upload_script.js"></script>
	<script type="text/javascript">
	function ajaxFileUpload()
	{
		$("#loading")
		.ajaxStart(function(){
			$(this).show();
		})
		.ajaxComplete(function(){
			$(this).hide();
		});

		$.ajaxFileUpload
		(
			{
				url:'dofileupload.php',
				secureuri:false,
				fileElementId:'fileToUpload',
				dataType: 'json',
				data:{name:'logan', id:'id'},
				success: function (data, status)
				{
					if(typeof(data.error) != 'undefined')
					{
						if(data.error != '')
						{
							alert(data.error);
						}else
						{
							alert(' File Upload Result:\n'+data.msg);
						}
					}
				},
				error: function (data, status, e)
				{
					alert(e);
				}
			}
		)
		
		return false;

	}
	</script>	
	</head>

	<body>
<div id="wrapper">
    <div id="content">
    	<h1>File Upload Page:</h1>
    
			  	
		
		<form name="form" action="" method="POST" enctype="multipart/form-data">
		<table cellpadding="0" cellspacing="0" class="tableForm">

		<thead>
			<tr>
				<th>Please select a file and click Upload button</th>
			</tr>
		</thead>
		<tbody>	

			<tr>
				<td><input id="fileToUpload" type="file" size="45" name="fileToUpload" class="input"></td>			
			</tr>

		</tbody>
			<tfoot>
				<tr>
					<td><button class="button" id="buttonUpload" onclick="return ajaxFileUpload();">Upload</button></td>
				</tr>
			</tfoot>
	
	</table>
		</form> 
		<p> List of all uploaded files can be found <a href="get_upload_file_list.php" target="_blank">here.</a></p>
		<p>&#11;&#11;</p>
		<img id="loading" src="loading.gif" style="display:none;">   
		<p><a href="logout.php">Logout</a></p>	
    </div>
    

	</body>
</html>