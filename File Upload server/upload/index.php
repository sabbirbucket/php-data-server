<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
	<title></title>
	
	<link rel="stylesheet" type="text/css" href="css/style.css" media="all" />
	
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.6/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/jquery.inputfocus-0.9.min.js"></script>
    <script type="text/javascript" src="js/jquery.main.js"></script>



<script type="text/javascript">
$("#add_err").html(" ");
$(document).ready(function(){
	
   $("#form_login").submit(function(){
    
        username=$("#username").val();
        password=$("#password").val();
       //alert(username+password);
         $.ajax({
            type: "POST",
            url: "login.php",
            data: "name="+username+"&pwd="+password,
            success: function(html){
              if(html=='true')
              {
                $("#container").fadeOut("normal");
                   window.location="home.php";						
              }
              else
              {
                    $("#add_err").html("<span style='color:red;font-size:15px'>Wrong username or password</span>");
              }
            },
            beforeSend:function()
			{
                 $("#add_err").html("<span style='font-size:15px'>Loading...</span>")
            }
        });
         return false;
    });
});
</script>


</head>
<body>
	
	<div id="container">
      <div id="outside">
        <div class="inside">
        <form action="#" id="form_login" method="post">
	
            <!-- #first_step -->
            <div id="first_step"><br/>
                <h1><span>Upload</span>Login</h1>

                <div class="form">
                    <input type="text" name="username" id="username" value="username" />
                    <label for="username">Enter Username</label>
                    
                    <input type="password" name="password" id="password" value="password" />
                    <label for="password">Enter Password</label>       
                  
                  </div>      <!-- clearfix --><div class="clear"></div><!-- /clearfix -->
                <input class="submit" type="submit" name="submit_first" id="submit_first" value="" /><br/><br/><br/>
             <div class="err" id="add_err" style="float:left;margin-left:-60px"></div>
            </div>      <!-- clearfix --><div class="clear"></div><!-- /clearfix -->
            
            
        </form>
 </div>
    </div>    
	</div>
	
</body>
</html>